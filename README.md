# Vehicle Tracking System Backend

This is a backend that makes use of rest APIs.

## Documentation

Click [here](/docs/) for documentation.

## Installation 

```
pip install -r requirements.txt
```
## Running

```
python manage.py runserver
```


## Setup

```
python manage.py migrate
```


## Create Superuser

```
python manage.py createsuperuser
```